package com.po.filemanager.dto;

import com.po.filemanager.entities.FileStorage;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class FileStorageResponse {
   FileStorage fileStorage;
   public FileStorageResponse(){}


   public FileStorage getFileStorage() {
       return fileStorage;
   }

   public void setFileStorage(FileStorage fileStorage) {
       this.fileStorage = fileStorage;
   }

}
