package com.po.filemanager.controller;

import com.po.filemanager.dto.FileStorageResponse;
import com.po.filemanager.entities.FileStorage;
import com.po.filemanager.exception.FileStorageException;
import com.po.filemanager.repository.FileStorageRepository;
import com.po.filemanager.service.impl.FileStorageServiceImpl;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("${controller.fileStorage}")
@CrossOrigin
@Api(tags = "Controller", value = "FileStorageController")
public class FileStorageController {

    @Value("${controller.fileStorage}")
    String fileStorage;

    private static final Logger logger = LoggerFactory.getLogger(FileStorageController.class);

    @Autowired
    FileStorageRepository fileStorageRepository;

    @Autowired
    FileStorageServiceImpl fileStorageServiceImpl;

    @PostMapping(value = "/uploadFile",consumes = {"multipart/form-data"})
    public ResponseEntity<FileStorageResponse> uploadFile(@RequestParam("file") MultipartFile file) throws FileStorageException {
        String fileName = fileStorageServiceImpl.storeFile(file);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(fileStorage + "downloadFile/")
                .path(fileName)
                .toUriString();
        FileStorage fileStorage = new FileStorage(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());

        FileStorageResponse fileStorageResponse = new FileStorageResponse();
        fileStorageResponse.setFileStorage(fileStorage);
        fileStorageRepository.save(fileStorage);
        return new ResponseEntity<FileStorageResponse>( fileStorageResponse, HttpStatus.OK );
    }

    /*@PostMapping("/uploadMultipleFiles")
    public List<ResponseEntity<FileStorageResponse>> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> {
                    try {
                        return uploadFile(file);
                    } catch (FileStorageException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }*/

    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) throws FileStorageException {
        // Load file as Resource
        Resource resource = fileStorageServiceImpl.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @GetMapping(value = "/")
    public ResponseEntity<List<FileStorage>> getAll() {
           return new ResponseEntity( fileStorageRepository.findAll(), HttpStatus.OK );
    }

}
