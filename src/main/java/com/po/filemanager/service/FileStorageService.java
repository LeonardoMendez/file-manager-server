package com.po.filemanager.service;

import com.po.filemanager.exception.FileStorageException;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface FileStorageService {

    public String storeFile(MultipartFile file) throws FileStorageException;
    public Resource loadFileAsResource(String fileName) throws FileStorageException;
}
