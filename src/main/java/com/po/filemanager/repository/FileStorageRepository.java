package com.po.filemanager.repository;

import com.po.filemanager.dto.FileStorageResponse;
import com.po.filemanager.entities.FileStorage;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FileStorageRepository extends MongoRepository<FileStorage,String> {
}
